//Soal A
function AscendingTen(num) {
	var angka =""
	if(num == null)
	{
		return -1
	}
	for (var i = num; i < num + 10; i++)
	{
		angka = angka + (i + " ")
		//var n = i.toString();
	}
	return angka;
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

//Soal B
function DescendingTen(num) {
	var angka =""
	if(num == null)
	{
		return -1
	}
	for (var i = num; i > num - 10; i--)
	{
		angka = angka + (i + " ")
		//var n = i.toString();
	}
	return angka;
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


//Soal C
function ConditionalAscDesc(reference,check)
{
	if(check == null || reference == null)
	{
		return -1
	}
	else if(check % 2 == 0)
	{
		var angka =""
		if(reference == null)
		{
			return -1
		}
		for (var i = reference; i < reference + 10; i++)
		{
			angka = angka + (i + " ")
			//var n = i.toString();
		}
		return angka;
		}

	else if (check % 2 != 0)
	{
		var angka =""
		if(reference == null)
		{
			return -1
		}
		for (var i = reference; i > reference - 10; i--)
		{
			angka = angka + (i + " ")
			//var n = i.toString();
		}
		return angka;
	}
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

//Soal D
function ularTangga()
{
	var k = 100;
	
	for(var i = 10; i >= 1; i--)
	{
		var angka = "";
		for(var j = 10; j>=1;j--)
		{
			if(i % 2 != 0)
			{
				angka = k + " "+angka;
			}
			else
			{
				angka = angka +k+" ";
			}
			k = k -1;
		}
		console.log(angka);
	}
	return ularTangga;
}
console.log(ularTangga());