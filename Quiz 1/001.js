//Soal A
function balikString(str)
{
	var newString ="";

	for(i = str.length - 1; i >= 0; i--)
	{
		newString += str[i];
	}

	return newString;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

//Soal B
function palindrome (kata) {
  return kata == kata.split('').reverse().join('');
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

//Soal C
function bandingkan(number,asli)
{
	if(number < 0 || asli < 0 || number == asli)
	{
		return -1
	}
	else if(number == null)
	{
		return asli
	}
	else if(asli == null)
	{
		return number
	}
	else
	{
		if(number > asli)
		{
			return number
		}
		else
		{
			return asli
		}
	}

}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18