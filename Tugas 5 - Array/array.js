//Soal No 1
function range(startNum,finishNum)
{
	if(startNum == null || finishNum == null)
	{
		return -1
	}

	else
	{
		var angka = [];
		if(startNum < finishNum)
		{
			for(var i = startNum; i <= finishNum; i++)
			{
				angka.push(i)
			}
			return angka;
		}
		else if(startNum > finishNum)
		{
			for(var j = startNum; j >= finishNum; j--)
			{
				angka.push(j)
			}
			return angka;
		}
	}
}

console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50))
console.log(range())

//Soal No 2

function rangeWithStep(startNum,finishNum,step)
{
	var angka = [];
		if(startNum < finishNum)
		{
			for(var i = startNum; i <= finishNum; i++)
			{
				angka.push(i)
				i = i + step - 1;
				
			}
			return angka;
		}
		else if(startNum > finishNum)
		{
			for(var j = startNum; j >= finishNum; j--)
			{
				angka.push(j)
				j = j - step + 1;
				
			}
			return angka;
		}
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

//Soal No 3
function sum(awal,akhir,step = 1)
{
	if(awal == null && akhir == null)
	{
		return 0;
	}
	else if (awal == null)
	{
		return akhir;
	}
	else if(akhir == null)
	{
		return awal;
	}
	else
	{
		var angka = 0;
		if(awal < akhir)
		{
			for(var i = awal; i <= akhir; i++)
			{
				angka = angka + i;
				i = i + step - 1;
				
			}
			return angka;
		}
		else if(awal > akhir)
		{
			for(var j = awal; j >= akhir; j--)
			{
				angka = angka + j;
				j = j - step + 1;
				
			}
			return angka;
		}
	}
	
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal No 4

var data = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

function dataHandling()
{
	var jawaban2 ="";
         for(var i =0; i<data.length; i++)
         {
             for (var j =0; j < data[i].length; j++)
            {
                 if (j == 0)
                 {
                     jawaban2 += "Nomor ID :"+data[i][j]+"\n";
                 }
                else if (j ==1)
                {
                    jawaban2 += "Nama Lengkap:"+data[i][j]+"\n";

                } 
                else if (j ==2)
                {
                    jawaban2 += "TTL :"+data[i][j]+" ";

                }
                else if (j ==3)
                {
                 jawaban2 += data[i][j] + "\n";

                }
                else if ( j == 4 )
                {
                    jawaban2 += "Hobi  :"+data[i][j]+"\n";

                }
            }
         
         jawaban2 += "\n";
	       }
        return jawaban2;
     
}
var output = dataHandling();
console.log(output);


//Soal No 5
function balikKata(str)
{
	var newString ="";

	for(i = str.length - 1; i >= 0; i--)
	{
		newString += str[i];
	}

	return newString;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode")); 
console.log(balikKata("Haji Ijah")); 
console.log(balikKata("racecar")); 
console.log(balikKata("I am Sanbers"));

//Soal No 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
function dataHandling2(input)
{
	var inputbaru = "";
	input.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung","21/05/1989","Pria","SMA Internasional Metro")
	input.splice();
	console.log(input);
	var simpan = input[3];

	var bagi = simpan.split("/");
	var bulan = bagi[1];
	switch (bulan) 
	{
	case "01" : {console.log('Januari'); break;}
	case "02" : {console.log('Februari'); break;}
	case "03" : {console.log('Maret'); break;}
	case "04" : {console.log('April'); break;}
	case "05" : {console.log('Mei'); break;}
	case "06" : {console.log('Juni'); break;}
	case "07" : {console.log('Juli'); break;}
	case "08" : {console.log('Agustus'); break;}
	case "09" : {console.log('September'); break;}
	case "10" : {console.log('Oktober'); break;}
	case "11": {console.log('Nopember'); break;}
	case "12" : {console.log('Desember'); break;}
	}
	


	bagi.sort(function(a, b){return b-a});
	console.log(bagi);

	var cetak = simpan.split("/");
	var gabung = cetak.join("-")
	console.log(gabung);


}

dataHandling2(input);