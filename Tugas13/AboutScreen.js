import React,{Component} from 'react';
import { StyleSheet, Text, View, Image,ScrollView } from 'react-native';
import { Octicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';

export default class App extends Component {
  render() {
    return (
      
      <View style={styles.container}>
          <Text style={styles.text}>Tentang Saya</Text>
          
          <View style = {styles.circle} />
          <Octicons style = {styles.person} name="person" size={100} color="gray" />
          <Text style={styles.text2}>Leslie Alexander</Text>
          <Text style={styles.text3}>React Native Developer</Text>

          <View style={styles.box} />
          <Text style={styles.text4}>Portofolio</Text>
          <View style={styles.line} />

          <FontAwesome name="gitlab" style = {styles.gitlab} size={60} color="#3EC6FF" />
          <AntDesign name="github" style = {styles.github} size={60} color="#3EC6FF" />
          <Text style={styles.text5}>@leslie90</Text>
          <Text style={styles.text6}>@leslie_a</Text>

          <View style={styles.box2} />

          <Text style={styles.text7}>Hubungi Saya</Text>
          <View style={styles.line2} />

          <Entypo name="facebook" style = {styles.facebook} size={45} color="#3EC6FF" />
          <Entypo name="twitter" style = {styles.twitter} size={45} color="#3EC6FF" />
          <AntDesign name="instagram" style = {styles.instagram} size={45} color="#3EC6FF" />

          <Text style={styles.text9}>@leslie_alex</Text>
          <Text style={styles.text10}>@leslie_aleeex</Text>
          <Text style={styles.text11}>@leslie90</Text>
          
      </View>
      
      
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  person: {
    position : 'absolute',
    left : 167,
    right : 93,
    top : 140,
  },

  circle: {
    width: 130,
    height: 130,
    borderRadius: 140/2,
    backgroundColor: '#EFEFEF',
    left : 140,
    top : 130,

  },

  line: {
    position : 'absolute',
    width: 360,
    height: 0,
    backgroundColor: '#003366',
    borderWidth : 1,
    left : 25,
    top : 393,
  },

  gitlab: {
    position : 'absolute',
    left : 60,
    top : 410,
  },

  github: {
    position : 'absolute',
    left : 290,
    top : 410,
  },

  facebook: {
    position : 'absolute',
    left : 100,
    top : 590,
  },

  twitter: {
    position : 'absolute',
    left : 100,
    top : 660,
  },

  instagram: {
    position : 'absolute',
    left : 100,
    top : 730,
  },

  /*Tentang Saya*/
  text: {
    position : 'absolute',
    width : 420,
    height : 62,
    left : 85,
    top : 64,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 36,
    lineHeight : 42,

    color : '#003366',
  },

  text2: {
    position : 'absolute',
    width : 300,
    height : 28,
    left : 106,
    top : 290,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 24,
    lineHeight : 28,

    color : '#003366',
  },

  text3: {
    position : 'absolute',
    width : 300,
    height : 19,
    left : 110,
    top : 320,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 16,
    lineHeight : 19,

    color : '#3EC6FF',
  },

  text4: {
    position : 'absolute',
    width : 100,
    height : 21,
    left : 25,
    top : 370,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    fontSize : 18,
    lineHeight : 21,

    color : '#003366',
  },

  text5: {
    position : 'absolute',
    width : 100,
    height : 25,
    left : 50,
    top : 485,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 16,
    lineHeight : 21,

    color : '#003366',
  },

  text6: {
    position : 'absolute',
    width : 100,
    height : 25,
    left : 281,
    top : 485,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 16,
    lineHeight : 21,

    color : '#003366',
  },

  text7: {
    position : 'absolute',
    width : 200,
    height : 21,
    left : 25,
    top : 549,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    fontSize : 18,
    lineHeight : 20,

    color : '#003366',
  },

  text9: {
    position : 'absolute',
    width : 200,
    height : 21,
    left : 180,
    top : 600,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 18,
    lineHeight : 20,

    color : '#003366',
  },

  text10: {
    position : 'absolute',
    width : 200,
    height : 21,
    left : 180,
    top : 670,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 18,
    lineHeight : 20,

    color : '#003366',
  },

  text11: {
    position : 'absolute',
    width : 200,
    height : 21,
    left : 180,
    top : 740,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 18,
    lineHeight : 20,

    color : '#003366',
  },

  line2: {
    position : 'absolute',
    width: 360,
    height: 0,
    backgroundColor: '#003366',
    borderWidth : 1,
    left : 25,
    top : 572,
  },

  box: {
    position : 'absolute',
    width : 380,
    height : 160,
    top : 360,
    left : 15,
    backgroundColor : '#EFEFEF',
    
    borderRadius : 16,
  },

  box2: {
    position : 'absolute',
    width : 380,
    height : 250,
    top : 540,
    left : 15,
    backgroundColor : '#EFEFEF',
    
    borderRadius : 16,
  },

})