import React,{Component} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
          <Image
          style={styles.image}
          source={require('./images/logo.png')}
          />
        <Text style={styles.text}>Register</Text>
        <Text style={styles.text2}>Username</Text>
        <View style={styles.box} />

        <Text style={styles.text3}>Email</Text>
        <View style={styles.box2} />

        <Text style={styles.text4}>Password</Text>
        <View style={styles.box3} />

        <Text style={styles.text5}>Ulangi Password</Text>
        <View style={styles.box4} />

        <View style={styles.box5} />
        <Text style={styles.text6}>Daftar</Text>

        <Text style={styles.text7}>atau</Text>

        <View style={styles.box6} />
        <Text style={styles.text8}>Masuk</Text>
        </View>
        

      )
    }
  }




const styles = StyleSheet.create({
  image: {
    position : 'absolute',
    width : 375,
    height : 102,
    left : 0,
    top : 63,
    padding : 4,
    alignItems: 'center',
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth : 1,
  },

  text: {
    position : 'absolute',
    color: '#003366',
    fontSize: 24,
    width : 120,
    height : 50,
    left : 160,
    top : 220,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 28,
  },

/* Username*/
  text2: {
    position : 'absolute',
    color: '#003366',
    fontSize: 16,
    width : 90,
    height : 19,
    left : 55,
    top : 303,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },

  /*Email*/
  text3: {
    position : 'absolute',
    left : 55,
    color: '#003366',
    fontSize: 16,
    width : 50,
    height : 19,
    top : 393,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },
  
/*Password*/
  text4: {
    position : 'absolute',
    left : 55,
    color: '#003366',
    fontSize: 16,
    width : 80,
    height : 19,
    top : 477,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },

  text5: {
    position : 'absolute',
    left : 55,
    color: '#003366',
    fontSize: 16,
    width : 170,
    height : 30,
    top : 564,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },

  text6: {
    position : 'absolute',
    left : 178,
    color: '#FFFFFF',
    fontSize: 18,
    width : 80,
    height : 100,
    top : 670,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },

  text7: {
    position : 'absolute',
    left : 185,
    color: '#3EC6FF',
    fontSize: 18,
    width : 48,
    height : 100,
    top : 711,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },


  
  text8: {
    position : 'absolute',
    left : 175,
    color: '#FFFFFF',
    fontSize: 18,
    width : 90,
    height : 100,
    top : 750,

    fontFamily : 'Roboto',
    fontStyle : 'normal',
    fontWeight : 'normal',
    lineHeight : 19,
  },

  box: {
    position : 'absolute',
    width : 300,
    height : 48,
    top : 326,

    backgroundColor : '#FFFFFF',
    borderColor : '#003366',
    borderWidth : 1,
  },
  
  box2: {
    position : 'absolute',
    width : 300,
    height : 48,
    top : 413,

    backgroundColor : '#FFFFFF',
    borderColor : '#003366',
    borderWidth : 1,
  },

  box3: {
    position : 'absolute',
    width : 300,
    height : 48,
    top : 500,

    backgroundColor : '#FFFFFF',
    borderColor : '#003366',
    borderWidth : 1,
  },

  box4: {
    position : 'absolute',
    width : 300,
    height : 48,
    top : 587,

    backgroundColor : '#FFFFFF',
    borderColor : '#003366',
    borderWidth : 1,
  },

  box5: {
    position : 'absolute',
    width : 140,
    height : 40,
    top : 660,

    backgroundColor : '#003366',
    borderRadius : 16,
    
  },

  box6: {
    position : 'absolute',
    width : 140,
    height : 40,
    top : 740,

    backgroundColor : '#3EC6FF',
    borderRadius : 16,
    
  },
})