// Soal No 1

var angka = 2

console.log("LOOPING PERTAMA")
while(angka < 21)
{
	console.log(angka + ' - I love coding')
	angka += 2;
}

console.log("LOOPING KEDUA")
while(angka > 1)
{
	console.log(angka + ' - I will become a mobile developer')
	angka -= 2;
}

//Soal No 2
for(var i = 1 ; i < 21 ; i++)
{
	if(i % 2 != 0) //jika ganjil
	{
		if(i % 3 == 0)
		{
			console.log(i + ' - I Love Coding')
		}
		else
		{
			console.log(i + ' - Santai')
		}
	}
	else //jika genap
	{
		console.log(i + ' - Berkualitas')
	}
}

// Soal No 3
//rows
var persegi = ""
for(var i = 0 ; i < 4 ; i++)
{
	for(var j = 0 ; j < 8 ; j++)
	{
		persegi = persegi.concat("#")
	}
	persegi = persegi.concat("\n")
}
console.log(persegi)


//Soal No 4
var segitiga = ""
for(var i = 1 ; i <= 7 ; i++)
{
	
	for(var j = 1 ; j <= i ; j++)
	{
		segitiga = segitiga.concat("#");
	}
	segitiga = segitiga.concat("\n");
}

console.log(segitiga)


//Soal No 5
var catur = ""
for(var i = 1 ; i <= 8 ; i++)
{
	for(var j = 1 ; j <= 8; j++)
	{
		if((i + j)%2 == 0)
		{
			catur = catur.concat(" ")
		}
		else
		{
			catur = catur.concat("#")
		}
	}
	catur = catur.concat("\n")
}
console.log(catur)


