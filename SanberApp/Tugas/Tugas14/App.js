import React,{Component} from 'react';
import { StyleSheet, Text, View, Image,ScrollView,FlatList} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import skill from './skillData.json'
import { MaterialCommunityIcons } from '@expo/vector-icons';


export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      
          <Image
          style={styles.image}
          source={require('./images/logo.png')}
          />
          <Ionicons name="md-person" style = {styles.person} size={40} color="#3EC6FF" />
          <Text style={styles.text}>Hai,</Text>
          <Text style={styles.text2}>Leslie Alexander</Text>

          <Text style={styles.text3}>SKILL</Text>
          <View style={styles.line} />
        
        <ScrollView horizontal>
          <View style={styles.box} />
          <Text style={styles.text4}>Library/Framework</Text>
          <View style={styles.box2} />
          <Text style={styles.text5}>Bahasa Pemrograman</Text>
          <View style={styles.box3} />
          <Text style={styles.text6}>Teknologi</Text>
        </ScrollView>
        
        <View style={styles.boxLarge} />
        <MaterialCommunityIcons name="react" size={100} style={styles.reactnative} color="#003366" />
        <View style={styles.boxLarge2} />
        <MaterialCommunityIcons name="laravel" size={100} style={styles.laravel} color="#003366" />
        <View style={styles.boxLarge3} />
        <MaterialCommunityIcons name="language-javascript" size={100} style={styles.javascript} color="#003366" />
        <Text style={styles.text7}>React Native</Text>
        <Text style={styles.text8}>Laravel</Text>
        <Text style={styles.text9}>Javasript</Text>
        <Text style={styles.text10}>Library/Framework</Text>
        <Text style={styles.text11}>Library/Framework</Text>
        <Text style={styles.text12}>Bahasa Pemrograman</Text>
        <Text style={styles.text13}>50%</Text>
        <Text style={styles.text14}>100%</Text>
        <Text style={styles.text15}>30%</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollContainer: {
    paddingVertical: 20,
  },
  reactnative:{
    position:'absolute',
    left :20,
    right : '3.12%',
    top :270,
  },

  laravel:{
    position:'absolute',
    left :20,
    right : '3.12%',
    top :410,
  },

  javascript:{
    position:'absolute',
    left :20,
    right : '3.12%',
    top :560,
  },



  image: {
    position: 'absolute',
    width:187,
    height:51,
    left:187,
    top: 10,
  },

  person: {
    left : '9.38%',
    top: '9.38%',
  },

  text: {
    position: 'absolute',
    width:21,
    height : 14,
    left:80,
    top:78,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'normal',
    fontSize:12,
    lineHeight:14,
  },

  text2: {
    position: 'absolute',
    width:200,
    height : 19,
    left:80,
    top:95,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'normal',
    fontSize:15,
    lineHeight:14,
  },

  text3: {
    position: 'absolute',
    width:400,
    height : 100,
    left:16,
    top:150,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'normal',
    fontSize:36,
    lineHeight:42,
  },

  text4: {
    position: 'absolute',
    width:120,
    height : 100,
    left:18,
    top:176,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:10,
    lineHeight:14,
  },
  text5: {
    position: 'absolute',
    width:120,
    height : 100,
    left:153,
    top:176,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:9,
    lineHeight:14,
  },
  text6: {
    position: 'absolute',
    width:120,
    height : 100,
    left:306,
    top:176,

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:12,
    lineHeight:14,
  },

  text7: {
    position: 'absolute',
    width:200,
    height : 28,
    left:145,
    top:290,
    color:'#003366',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:24,
    lineHeight:28,
  },

  text8: {
    position: 'absolute',
    width:200,
    height : 28,
    left:145,
    top:430,
    color:'#003366',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:24,
    lineHeight:28,
  },

  text9: {
    position: 'absolute',
    width:200,
    height : 28,
    left:145,
    top:580,
    color:'#003366',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:24,
    lineHeight:28,
  },

  text10: {
    position: 'absolute',
    width:200,
    height : 30,
    left:145,
    top:320,
    color:'#3EC6FF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:16,
    lineHeight:19,
  },

  text11: {
    position: 'absolute',
    width:200,
    height : 30,
    left:145,
    top:460,
    color:'#3EC6FF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:16,
    lineHeight:19,
  },

  text12: {
    position: 'absolute',
    width:200,
    height : 30,
    left:145,
    top:610,
    color:'#3EC6FF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:16,
    lineHeight:19,
  },

  text13: {
    position: 'absolute',
    width:200,
    height : 56,
    left:145,
    top:329,
    color:'#FFFFFF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:40,
    lineHeight:56,
  },

  text14: {
    position: 'absolute',
    width:200,
    height : 56,
    left:145,
    top:470,
    color:'#FFFFFF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:40,
    lineHeight:56,
  },

  text15: {
    position: 'absolute',
    width:200,
    height : 56,
    left:145,
    top:620,
    color:'#FFFFFF',

    fontFamily:'Roboto',
    fontStyle : 'normal',
    fontWeight:'bold',
    fontSize:40,
    lineHeight:56,
  },


  line: {
    position : 'absolute',
    width: 360,
    height: 0,
    backgroundColor: '#3EC6FF',
    borderWidth : 1,
    left : 25,
    top : 200,
  },

  box: {
    width : 125,
    height : 32,
    top : 170,
    left:5,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor : '#B4E9FF',
   
    borderRadius : 8,
  },

  box2: {
    width : 125,
    height : 32,
    top : 170,
    left:5,
    marginBottom: 10,
    marginRight: 10,
   
    backgroundColor : '#B4E9FF',
    
    borderRadius : 8,
  },

  box3: {
    width : 125,
    height : 32,
    top : 170,
    marginBottom: 10,
    marginRight: 10,
    left:5,
 
    backgroundColor : '#B4E9FF',
    
    borderRadius : 8,
  },

  boxLarge: {
    position : 'absolute',
    width : 343,
    height : 129,
    top : 260,
    left : 15,
    marginBottom:10,
    backgroundColor : '#B4E9FF',
    
    borderRadius : 8,
  },

  boxLarge2: {
    position : 'absolute',
    width : 343,
    height : 129,
    top : 400,
    left : 15,
    marginBottom:10,
    backgroundColor : '#B4E9FF',
    
    borderRadius : 8,
  },

  boxLarge3: {
    position : 'absolute',
    width : 343,
    height : 129,
    top : 544,
    left : 15,
    marginBottom:10,
    backgroundColor : '#B4E9FF',
    
    borderRadius : 8,
  },




});